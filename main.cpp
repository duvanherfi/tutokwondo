#include <allegro.h>
#include <winalleg.h>
#include <stdio.h>



//funciones prototipo
void iniciarAllegro();


int main()
{
    iniciarAllegro();
//***********************variables*********************************
    bool salida=false;                                          //*
    int ind=1;                                                  //*
    int c=0;                                                    //*
    int d=0;
    int movimientos;                                            //*
    int movimientos2;                                            //*
    int x=581,y=396;
    int xp=42,yp=396;
    int puntos=0;
    int vidas=2;
    //*

    //buffer                                                    //*
    BITMAP *buffer = create_bitmap(800,600);                    //*
    BITMAP *buffer2 = create_bitmap(800,600);                   //*
    //*
    //***************************fondos**************           //*
    BITMAP *fondo=load_bitmap("im/fondo.bmp",NULL);             //*
    BITMAP *game_over=load_bitmap("im/gameover.bmp",NULL);
    BITMAP *ganar=load_bitmap("im/ganar.bmp",NULL);

    BITMAP *uno=load_bitmap("im/uno.bmp",NULL);
    BITMAP *dos=load_bitmap("im/dos.bmp",NULL);
    BITMAP *tres=load_bitmap("im/tres.bmp",NULL);
    BITMAP *cuatro=load_bitmap("im/cuatro.bmp",NULL);

    BITMAP *inicio=load_bitmap("im/inicio.bmp",NULL);           //*
    BITMAP *inicio2=load_bitmap("im/inicio2.bmp",NULL);         //*
    BITMAP *inicio3=load_bitmap("im/inicio3.bmp",NULL);         //*
    BITMAP *inicio4=load_bitmap("im/inicio4.bmp",NULL);         //*
    BITMAP *inicio5=load_bitmap("im/inicio5.bmp",NULL);         //*
    //cursor                                                    //*
    BITMAP *cursor=load_bitmap("im/cursor.bmp",NULL);           //*
    BITMAP *i1=load_bitmap("im/arreglo/i1.bmp",NULL);           //*
    BITMAP *i2=load_bitmap("im/arreglo/i2.bmp",NULL);           //*
    BITMAP *i3=load_bitmap("im/arreglo/i3.bmp",NULL);           //*
    BITMAP *i4=load_bitmap("im/arreglo/i4.bmp",NULL);           //*
    BITMAP *i5=load_bitmap("im/arreglo/i5.bmp",NULL);           //*
    BITMAP *i6=load_bitmap("im/arreglo/i6.bmp",NULL);           //*
    BITMAP *i7=load_bitmap("im/arreglo/i7.bmp",NULL);           //*
    BITMAP *i8=load_bitmap("im/arreglo/i8.bmp",NULL);           //*
    BITMAP *i9=load_bitmap("im/arreglo/i9.bmp",NULL);           //*
    BITMAP *i0=load_bitmap("im/arreglo/reco.bmp",NULL);         //*
    BITMAP *i10=load_bitmap("im/arreglo/jugar.bmp",NULL);       //*
    BITMAP *instruccion=load_bitmap("im/arreglo/i10.bmp",NULL); //*
    //***********arreglo inicio************************************
    BITMAP *inicial[]= {i0,i1,i2,i3,i4,i5,i6,i7,i8,i10};        //*
    //**********************personajes*****************************
    BITMAP *p1=load_bitmap("im/idle/p1.bmp",NULL);              //*
    //Personaje opuesto                                         //*
    BITMAP *bk1=load_bitmap("im/back_kick/opuesto1.bmp",NULL);  //*
    BITMAP *bk2=load_bitmap("im/back_kick/opuesto2.bmp",NULL);  //*
    BITMAP *bk3=load_bitmap("im/back_kick/opuesto3.bmp",NULL);  //*
    BITMAP *bk4=load_bitmap("im/back_kick/opuesto4.bmp",NULL);
    //*
    BITMAP *down1=load_bitmap("im/down/opuesto1.bmp",NULL);     //*
    BITMAP *down2=load_bitmap("im/down/opuesto2.bmp",NULL);     //*
    BITMAP *down3=load_bitmap("im/down/opuesto3.bmp",NULL);
    //*
    BITMAP *jump1=load_bitmap("im/jump/opuesto4.bmp",NULL);     //*
    BITMAP *jump2=load_bitmap("im/jump/opuesto3.bmp",NULL);     //*
    BITMAP *jump3=load_bitmap("im/jump/opuesto2.bmp",NULL);     //*
    BITMAP *jump4=load_bitmap("im/jump/opuesto1.bmp",NULL);

    BITMAP *punch1=load_bitmap("im/punch/1epunch.bmp",NULL);
    BITMAP *punch2=load_bitmap("im/punch/2epunch.bmp",NULL);
    BITMAP *punch3=load_bitmap("im/punch/4epunch.bmp",NULL);
    BITMAP *punch4=load_bitmap("im/punch/3epunch.bmp",NULL);

    BITMAP *upper1=load_bitmap("im/punch upper/1epunch.bmp",NULL);
    BITMAP *upper2=load_bitmap("im/punch upper/2epunch.bmp",NULL);
    BITMAP *upper3=load_bitmap("im/punch upper/4epunch.bmp",NULL);
    BITMAP *upper4=load_bitmap("im/punch upper/3epunch.bmp",NULL);

    BITMAP *kicka1=load_bitmap("im/kick axe/1ekick axe.bmp",NULL);
    BITMAP *kicka2=load_bitmap("im/kick axe/2ekick axe.bmp",NULL);
    BITMAP *kicka3=load_bitmap("im/kick axe/3ekick axe.bmp",NULL);
    BITMAP *kicka4=load_bitmap("im/kick axe/4ekick axe.bmp",NULL);
    BITMAP *kicka5=load_bitmap("im/kick axe/5ekick axe.bmp",NULL);

    BITMAP *kick1=load_bitmap("im/kick/1ekick.bmp",NULL);
    BITMAP *kick2=load_bitmap("im/kick/2ekick.bmp",NULL);
    BITMAP *kick3=load_bitmap("im/kick/3ekick.bmp",NULL);

    BITMAP *kickdown1=load_bitmap("im/kick down/1ekick down.bmp",NULL);
    BITMAP *kickdown2=load_bitmap("im/kick down/2ekick down.bmp",NULL);
    //*
    //arreglo mov1                                              //*
    BITMAP *mov1[]= {bk4,bk3,bk2,bk1};                          //*
    BITMAP *mov2[]= {down3,down1,down2};                        //*
    BITMAP *mov3[]= {jump1,jump2,jump3,jump4};
    BITMAP *mov4[]= {punch1,punch2,punch3,punch4};
    BITMAP *mov5[]= {upper1,upper2,upper3,upper4};
    BITMAP *mov6[]= {kicka1,kicka2,kicka3,kicka4,kicka5};
    BITMAP *mov7[]= {kick1,kick2,kick3};
    BITMAP *mov8[]= {kickdown1,kickdown2};

    //Personaje Principal
    BITMAP *bk1p=load_bitmap("im/back_kick/1back kick.bmp",NULL);
    BITMAP *bk2p=load_bitmap("im/back_kick/2back kick.bmp",NULL);                                                            //*
    BITMAP *bk3p=load_bitmap("im/back_kick/3back kick.bmp",NULL);
    BITMAP *bk4p=load_bitmap("im/back_kick/4back kick.bmp",NULL);

    BITMAP *down1p=load_bitmap("im/down/1down.bmp",NULL);
    BITMAP *down2p=load_bitmap("im/down/2down.bmp",NULL);
    BITMAP *down3p=load_bitmap("im/down/3down.bmp",NULL);

    BITMAP *jump1p=load_bitmap("im/jump/1jump.bmp",NULL);
    BITMAP *jump2p=load_bitmap("im/jump/2jump.bmp",NULL);
    BITMAP *jump3p=load_bitmap("im/jump/3jump.bmp",NULL);
    BITMAP *jump4p=load_bitmap("im/jump/4jump.bmp",NULL);

    BITMAP *punch1p=load_bitmap("im/punch/1punch.bmp",NULL);
    BITMAP *punch2p=load_bitmap("im/punch/2punch.bmp",NULL);
    BITMAP *punch3p=load_bitmap("im/punch/4punch.bmp",NULL);
    BITMAP *punch4p=load_bitmap("im/punch/3punch.bmp",NULL);

    BITMAP *upper1p=load_bitmap("im/punch upper/1punch.bmp",NULL);
    BITMAP *upper2p=load_bitmap("im/punch upper/2punch.bmp",NULL);
    BITMAP *upper3p=load_bitmap("im/punch upper/4punch.bmp",NULL);
    BITMAP *upper4p=load_bitmap("im/punch upper/3punch.bmp",NULL);

    BITMAP *kicka1p=load_bitmap("im/kick axe/1kick axe.bmp",NULL);
    BITMAP *kicka2p=load_bitmap("im/kick axe/2kick axe.bmp",NULL);
    BITMAP *kicka3p=load_bitmap("im/kick axe/3kick axe.bmp",NULL);
    BITMAP *kicka4p=load_bitmap("im/kick axe/4kick axe.bmp",NULL);
    BITMAP *kicka5p=load_bitmap("im/kick axe/5kick axe.bmp",NULL);

    BITMAP *kick1p=load_bitmap("im/kick/1kick.bmp",NULL);
    BITMAP *kick2p=load_bitmap("im/kick/2kick.bmp",NULL);
    BITMAP *kick3p=load_bitmap("im/kick/3kick.bmp",NULL);

    BITMAP *kickdown1p=load_bitmap("im/kick down/1kick down.bmp",NULL);
    BITMAP *kickdown2p=load_bitmap("im/kick down/2kick down.bmp",NULL);

    BITMAP *idle1=load_bitmap("im/idle/1idle.bmp",NULL);
    BITMAP *idle2=load_bitmap("im/idle/2idle.bmp",NULL);
    BITMAP *idle3=load_bitmap("im/idle/3idle.bmp",NULL);

    //arreglo pj principal
    BITMAP *mov1p[]= {bk4p,bk3p,bk2p,bk1p};
    BITMAP *mov2p[]= {down3p,down1p,down2p};
    BITMAP *mov3p[]= {jump4p,jump3p,jump2p,jump1p};
    BITMAP *mov4p[]= {punch1p,punch2p,punch3p,punch4p};
    BITMAP *mov5p[]= {upper1p,upper2p,upper3p,upper4p};
    BITMAP *mov6p[]= {kicka1p,kicka2p,kicka3p,kicka4p,kicka5p};
    BITMAP *mov7p[]= {kick1p,kick2p,kick3p};
    BITMAP *mov8p[]= {kickdown1p,kickdown2p};
    BITMAP *idle[]= {idle1,idle2,idle3};
    BITMAP *conteo[]= {cuatro,uno,dos,tres};

    //arreglos combos
    BITMAP *combo1[]= {kickdown1,kickdown2,upper1,upper2,upper3,upper4};
    BITMAP *combo1p[]= {kickdown1p,kickdown2p,upper1p,upper2p,upper3p,upper4p};


    BITMAP *combo2[]= {bk4,bk3,bk2,bk1,punch1,punch2,punch3,punch4};
    BITMAP *combo2p[]= {bk4p,bk3p,bk2p,bk1p,punch1p,punch2p,punch3p,punch4p};


    BITMAP *combo3[]= {upper1,upper2,upper3,upper4,kicka1,kicka2,kicka3,kicka4,kicka5};
    BITMAP *combo3p[]= {upper1p,upper2p,upper3p,upper4p,kicka1p,kicka2p,kicka3p,kicka4p,kicka5p};

    //*
    //*
    //*
//************************************************************************************
    clear_to_color(buffer, 0xFFFFFF);//pone el buffer de color blanco
    while(!salida) //inicia el while de todo el programa
    {

        if (mouse_x > 165 && mouse_x < 690 && mouse_y > 260 && mouse_y < 305)
        {
            //condicion que al pasar el mouse por estas coordenadas  muestra en rojo la palabra instruciones
            blit(inicio3,buffer,0,0,0,0,800,600);
            masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,13,22);//pinta a flecha del mouse
            blit(buffer,screen,0,0,0,0,800,600);

            if (mouse_b&1)
            {
                //si se le da click ejecuta el sgte while
                while(!key[KEY_ESC]) //while que muestra repetidamente la imagen instrucciones hasta que se le presione esc
                {
                    blit(instruccion,buffer,0,0,0,0,800,600);
                    blit(buffer,screen,0,0,0,0,800,600);
                }

            }

        }

        else if (mouse_x > 210 && mouse_x < 586 && mouse_y > 356 && mouse_y < 396)
        {
            //condicion que al pasar el mouse por estas coordenadas muestra en rojo la palabra acerca de
            blit(inicio4,buffer,0,0,0,0,800,600);
            masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,13,22);
            blit(buffer,screen,0,0,0,0,800,600);

            if (mouse_b&1)
            {
                //si se le da click ejecuta la sgte instuccion
                while(!key[KEY_ESC])
                {
                    //while que muestra imagen acerca de. hasta que se le presione esc
                    blit(i9,buffer,0,0,0,0,800,600);
                    masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,13,22);
                    blit(buffer,screen,0,0,0,0,800,600);
                }

            }


        }
        else if (mouse_x > 305 && mouse_x < 486 && mouse_y > 435 && mouse_y < 515)
        {
            //condicion que al pasar el mouse por estas coordenadas muestra en rojo la palabra salir
            blit(inicio5,buffer,0,0,0,0,800,600);
            masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,13,22);
            blit(buffer,screen,0,0,0,0,800,600);

            if (mouse_b&1)
            {
                //si de le da click vuelve salida verdadera y ya saldria del while principal
                salida=true;
                return 0;
            }
        }

        else if(key[KEY_END])
        {
            //condicion que si se presiona la tecla fin vuelve salida verdadera y saldria del while principal
            salida=true;
        }

        else if (mouse_x > 294 && mouse_x < 520 && mouse_y > 171 && mouse_y < 226)
        {
            //condicion que al pasar el mouse por estas coordenadas muestra en rojo la palabra jugar
            blit(inicio2,buffer,0,0,0,0,800,600);
            masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,13,22);
            blit(buffer,screen,0,0,0,0,800,600);

            if (mouse_b&1)
            {
                //si se le da click en esta posicionn iniciara el juego
                rest(40);


                while(!key[KEY_ESC])
                {

                    //while que contiene las instruciones del juego que empieza con las imagenes de los movimientos

                    if(c==0)
                    {
                        //condicion que muestra una recomendacion al inicio de las imagnes
                        blit(inicial[0],buffer,0,0,0,0,800,600);
                        blit(buffer,screen,0,0,0,0,800,600);
                        c =1;
                        rest(3000);//espera tres segundos
                    }

                    blit(inicial[ind],buffer,0,0,0,0,800,600);//muestra las imagenes del arreglo de acuerdo al indice en el buffer
                    blit(buffer,screen,0,0,0,0,800,600);


                    if(key[KEY_RIGHT]&& ind<9)
                    {
                        //simepre y cuando ind sea menor a nueve y se presione la tecla derecha avanzara a la siguiente imagen en el arreglo
                        ind++;
                        rest(145);
                    }

                    if(key[KEY_LEFT]&& ind>1)
                    {
                        //simepre y cuando ind sea mayor a uno y se presione la tecla izquierda retrocedera a la anterior imagen en el arreglo
                        ind--;
                        rest(145);
                    }
                    //despues de que se termine el arreglo mostrara una imagen para comenzar la practica de lo anterior mostrado
//******************************************************************practica de movimientos***********************************************************
                    if((ind==9) && (key[KEY_ENTER]))
                    {
                        //si el indice es igual a 9 y se presiona enter entrara a la practica del juego con los movimientos mostrados
                        while(!key[KEY_ESC])
                        {
                            //while que ejecuta los movimientos
                            srand(time(NULL));//funcion que caambia el patron del rand
                            clear(buffer);//se limpia el buffer para evitar que se muestre alguna imagen anterior.
                            movimientos=rand()%8;//genera numeros aleatorios del 0 al 7

                            if(vidas==-1)
                            {
                                //condicion de vida para que se termine el juego
                                while(!key[KEY_ENTER])
                                {
                                    //while que muestra repetidamente la pantalla game over hasta que se presione enter.

                                    for(int i=0; i<3; i++)
                                    {
                                        //este ciclo muestra repetidamente el movimiento del personaje
                                        blit(game_over,buffer,0,0,0,0,800,600);
                                        draw_sprite(buffer,idle[i],293,299);
                                        blit(buffer,screen,0,0,0,0,800,600);


                                        if(key[KEY_ENTER])
                                        {
                                            //si se presiona enter rompe dos veces para volver al menu.
                                            break;
                                            break;
                                        }
                                        rest(120);
                                    }

                                }
                                break;
                                break;
                            }
                            if(d==0)
                            {
                                //condicion que muestra el conteo con un arreglo de imagenes simepre y cuando d=0
                                for(int i=3; i>-1; i--)
                                {
                                    blit(fondo,buffer2,0,0,0,0,800,600);
                                    draw_sprite(buffer2,p1,xp,yp);
                                    draw_sprite(buffer2,mov1[0],x,y);
                                    draw_sprite(buffer2,conteo[i],320,289);
                                    blit(buffer2,screen,0,0,0,0,800,600);
                                    rest(1000);
                                }
                                d++;
                            }

                            blit(fondo,buffer2,0,0,0,0,800,600);//imprimimos el fondo en el buffer2
                            text_mode(-1);//pone el texto que se imprimira en el textprintf transparente
                            textprintf(buffer2,font,239,28,0,"% d",puntos);//imprimimos la variable puntos en su respectivas oordenadas
                            textprintf(buffer2,font,620,28,0,"% d",vidas);//imprimimos la variable vidas en su respectivas oordenadas

                            draw_sprite(buffer2,p1,xp,yp);//imprimimos el personaje 1 en el buffer2;



                            if(movimientos==0) //si movimientos =0 eecuta el primier movimiento
                            {
                                for(int i=0; i<4; i++)
                                {


                                    blit(buffer2,buffer,0,0,0,0,800,600);//imprimimos el bufer2 en el buffer para que muestre ambos personajes despues
                                    draw_sprite(buffer,mov1[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==2 || i==3) rest(350);
                                    else rest(300);
                                }



                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov1[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);


                                while(true)
                                {
                                    if(key[KEY_Z])
                                    {
                                        if(key[KEY_LEFT])
                                        {

                                            puntos+=10;

                                            for(int i=0; i<4; i++)
                                            {
                                                blit(fondo,buffer,0,0,0,0,800,600);
                                                draw_sprite(buffer,mov1p[i],xp,yp);
                                                draw_sprite(buffer,mov1[0],x,y);
                                                blit(buffer,screen,0,0,0,0,800,600);
                                                if(i==2 || i==3) rest(350);
                                                else rest(300);
                                            }

                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov1p[0],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            break;
                                        }





                                    }
                                    if(key[KEY_ESC])
                                    {

                                        break;
                                        break;
                                    }
                                    if(key[KEY_X])
                                    {
                                        vidas--;
                                        break;
                                    }
                                }
                            }

                            if(movimientos==1)
                            {

                                for(int i=0; i<3; i++)
                                {

                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,mov2[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==0) rest(370);
                                    if(i==1)rest(380);
                                    else rest(390);
                                }


                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov1[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);


                                while(true)
                                {
                                    if(key[KEY_DOWN])
                                    {
                                        puntos+=10;
                                        for(int i=0; i<3; i++)
                                        {
                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov2p[i],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            if(i==0) rest(370);
                                            if(i==1)rest(380);
                                            else rest(390);
                                        }
                                        blit(fondo,buffer,0,0,0,0,800,600);
                                        draw_sprite(buffer,mov2p[0],xp,yp);
                                        draw_sprite(buffer,mov1[0],x,y);
                                        blit(buffer,screen,0,0,0,0,800,600);
                                        break;
                                    }
                                    if(key[KEY_RIGHT] || key[KEY_UP] || key[KEY_LEFT])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_ESC])
                                    {
                                        break;
                                        break;
                                    }
                                }
                            }

                            if(movimientos==2)
                            {

                                for(int i=0; i<4; i++)
                                {
                                    if(i==1) y-=100;
                                    if(i==2) y-=50;
                                    if(i==3)
                                    {
                                        x=581;
                                        y=386;
                                    }

                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,mov3[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    rest(380);
                                }


                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov1[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);


                                while(true)
                                {
                                    if(key[KEY_UP])
                                    {
                                        puntos+=10;
                                        for(int i=0; i<4; i++)
                                        {
                                            if(i==1) yp-=100;
                                            if(i==2) yp-=50;
                                            if(i==3)
                                            {
                                                xp=42;
                                                yp=386;
                                            }
                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov3p[i],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            rest(380);
                                        }

                                        blit(fondo,buffer,0,0,0,0,800,600);
                                        draw_sprite(buffer,mov3p[0],xp,yp);
                                        draw_sprite(buffer,mov1[0],x,y);
                                        blit(buffer,screen,0,0,0,0,800,600);
                                        break;
                                    }
                                    if(key[KEY_RIGHT] || key[KEY_DOWN] || key[KEY_LEFT])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_ESC])
                                    {
                                        break;
                                        break;
                                    }
                                }



                            }

                            if(movimientos==3)
                            {
                                for(int i=0; i<4; i++)
                                {


                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,mov4[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==2 || i==3) rest(150);
                                    else rest(150);
                                }



                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov1[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);


                                while(true)
                                {
                                    if(key[KEY_X])
                                    {
                                        puntos+=10;
                                        for(int i=0; i<4; i++)
                                        {
                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov4p[i],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            if(i==2 || i==3) rest(350);
                                            else rest(300);
                                        }

                                        blit(fondo,buffer,0,0,0,0,800,600);
                                        draw_sprite(buffer,mov4p[0],xp,yp);
                                        draw_sprite(buffer,mov1[0],x,y);
                                        blit(buffer,screen,0,0,0,0,800,600);
                                        break;
                                    }
                                    if(key[KEY_Z])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_ESC])
                                    {

                                        break;
                                        break;
                                    }
                                }
                            }


                            if(movimientos==4)
                            {
                                for(int i=0; i<4; i++)
                                {


                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,mov5[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==2 || i==3) rest(200);
                                    else rest(200);
                                }



                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov1[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);


                                while(true)
                                {
                                    if(key[KEY_X])
                                    {
                                        if(key[KEY_UP])
                                        {
                                            puntos+=10;
                                            for(int i=0; i<4; i++)
                                            {
                                                blit(fondo,buffer,0,0,0,0,800,600);
                                                draw_sprite(buffer,mov5p[i],xp,yp);
                                                draw_sprite(buffer,mov1[0],x,y);
                                                blit(buffer,screen,0,0,0,0,800,600);
                                                if(i==2 || i==3) rest(350);
                                                else rest(300);
                                            }

                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov5p[0],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            break;
                                        }
                                    }
                                    if(key[KEY_Z])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_ESC])
                                    {

                                        break;
                                        break;
                                    }
                                }
                            }

                            if(movimientos==5)
                            {
                                for(int i=0; i<5; i++)
                                {


                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,mov6[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==2 || i==3  || i==4) rest(150);
                                    else rest(150);
                                }



                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov1[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);


                                while(true)
                                {
                                    if(key[KEY_Z])
                                    {
                                        if(key[KEY_UP])
                                        {
                                            rest(80);
                                            if(key[KEY_UP])
                                            {
                                                puntos+=10;
                                                for(int i=0; i<5; i++)
                                                {
                                                    blit(fondo,buffer,0,0,0,0,800,600);
                                                    draw_sprite(buffer,mov6p[i],xp,yp);
                                                    draw_sprite(buffer,mov1[0],x,y);
                                                    blit(buffer,screen,0,0,0,0,800,600);
                                                    if(i==2 || i==3) rest(350);
                                                    else rest(300);
                                                }

                                                blit(fondo,buffer,0,0,0,0,800,600);
                                                draw_sprite(buffer,mov6p[0],xp,yp);
                                                draw_sprite(buffer,mov1[0],x,y);
                                                blit(buffer,screen,0,0,0,0,800,600);
                                                break;
                                            }
                                        }
                                    }
                                    if(key[KEY_X])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_ESC])
                                    {

                                        break;
                                        break;
                                    }
                                }
                            }

                            if(movimientos==6)
                            {

                                for(int i=0; i<3; i++)
                                {
                                    // blit(fondo,buffer,0,0,0,0,800,600);
                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,mov7[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==0) rest(150);
                                    if(i==1)rest(150);
                                    else rest(150);
                                }

                                //blit(fondo,buffer,0,0,0,0,800,600);
                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov2[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);


                                while(true)
                                {
                                    if(key[KEY_Z])
                                    {
                                        if(key[KEY_RIGHT])
                                        {
                                            puntos+=10;
                                            for(int i=0; i<3; i++)
                                            {
                                                blit(fondo,buffer,0,0,0,0,800,600);
                                                draw_sprite(buffer,mov7p[i],xp,yp);
                                                draw_sprite(buffer,mov1[0],x,y);
                                                blit(buffer,screen,0,0,0,0,800,600);
                                                if(i==2 || i==3) rest(350);
                                                else rest(300);
                                            }

                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov7p[0],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            break;
                                        }
                                    }
                                    if(key[KEY_X])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_ESC])
                                    {
                                        break;
                                        break;
                                    }
                                }
                            }


                            if(movimientos==7)
                            {

                                for(int i=0; i<2; i++)
                                {
                                    // blit(fondo,buffer,0,0,0,0,800,600);
                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,mov8[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==0) rest(250);
                                    else rest(250);
                                }


                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov2[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);


                                while(true)
                                {
                                    if(key[KEY_Z])
                                    {
                                        if(key[KEY_DOWN])
                                        {
                                            puntos+=10;
                                            for(int i=0; i<2; i++)
                                            {
                                                blit(fondo,buffer,0,0,0,0,800,600);
                                                draw_sprite(buffer,mov8p[i],xp,yp);
                                                draw_sprite(buffer,mov1[0],x,y);
                                                blit(buffer,screen,0,0,0,0,800,600);
                                                if(i==2 || i==3) rest(350);
                                                else rest(300);
                                            }

                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov8p[0],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            break;
                                        }
                                    }
                                    if(key[KEY_X])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_ESC])
                                    {
                                        break;
                                        break;
                                    }
                                }
                            }
                            rest(40);//espera entre un movimiento y otro.
                            //segunda parte que incluye combos
                            if(puntos==70)
                            {
                                break;
                            }
                        }//finaliza while de primera parte de juego movimientos

                        while(!key[KEY_ESC])
                        {

                            if(vidas==-1 || puntos==200)
                            {
                                //condicion de vida para que se termine el juego
                                while(!key[KEY_ENTER])
                                {
                                    //while que muestra repetidamente la pantalla game over hasta que se presione enter.

                                    for(int i=0; i<3; i++)
                                    {
                                        //este ciclo muestra repetidamente el movimiento del personaje
                                        blit(ganar,buffer,0,0,0,0,800,600);
                                        if(puntos==200)
                                        {
                                            text_mode(0);
                                            textprintf(buffer,font,650,270,15,"% d",puntos);//imprimimos la variable puntos en su respectivas oordenadas
                                        }

                                        draw_sprite(buffer,idle[i],293,299);
                                        blit(buffer,screen,0,0,0,0,800,600);


                                        if(key[KEY_ENTER])
                                        {
                                            //si se presiona enter rompe dos veces para volver al menu.
                                            break;
                                            break;
                                        }
                                        rest(120);
                                    }

                                }
                                break;
                                break;
                            }
                            if(key[KEY_ESC])
                            {
                                break;
                                break;
                                break;

                            }

                            blit(fondo,buffer2,0,0,0,0,800,600);//imprimimos el fondo en el buffer2
                            text_mode(-1);//pone el texto que se imprimira en el textprintf transparente
                            textprintf(buffer2,font,239,28,0,"% d",puntos);//imprimimos la variable puntos en su respectivas oordenadas
                            textprintf(buffer2,font,620,28,0,"% d",vidas);//imprimimos la variable vidas en su respectivas oordenadas
                            draw_sprite(buffer2,p1,xp,yp);//imprimimos el personaje 1 en el buffer2;

                            movimientos2=rand()%3;

                            if(movimientos2==0)
                            {
                                for(int i=0; i<6; i++)
                                {
                                    // blit(fondo,buffer,0,0,0,0,800,600);
                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,combo1[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==1) rest(180);
                                    else rest(250);
                                }


                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov1[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);

                                if(key[KEY_ESC])
                                {
                                    break;
                                    break;
                                    break;

                                }

                                while(true)
                                {
                                    if(key[KEY_X])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_Z] && key[KEY_DOWN])
                                    {
                                        for(int i=0; i<2; i++)
                                        {
                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov8p[i],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            if(i==2 || i==3) rest(350);
                                            else rest(300);
                                        }
                                        blit(buffer2,buffer,0,0,0,0,800,600);
                                        //draw_sprite(buffer,mov8p[0],xp,yp);
                                        draw_sprite(buffer,mov1[0],x,y);
                                        blit(buffer,screen,0,0,0,0,800,600);

                                        while(true)
                                        {
                                            if(key[KEY_Z])
                                            {
                                                vidas--;
                                                break;
                                                break;
                                            }
                                            if(key[KEY_ESC])
                                            {
                                                break;
                                                break;
                                                break;
                                            }
                                            if(key[KEY_X])
                                            {
                                                if(key[KEY_UP])
                                                {
                                                    puntos+=10;
                                                    vidas+=1;
                                                    for(int i=0; i<4; i++)
                                                    {
                                                        blit(fondo,buffer,0,0,0,0,800,600);
                                                        draw_sprite(buffer,mov5p[i],xp,yp);
                                                        draw_sprite(buffer,mov1[0],x,y);
                                                        blit(buffer,screen,0,0,0,0,800,600);
                                                        if(i==2 || i==3) rest(350);
                                                        else rest(300);
                                                    }

                                                    blit(fondo,buffer,0,0,0,0,800,600);
                                                    draw_sprite(buffer,mov5p[0],xp,yp);
                                                    draw_sprite(buffer,mov1[0],x,y);
                                                    blit(buffer,screen,0,0,0,0,800,600);
                                                    break;
                                                }
                                            }
                                        }
                                        break;
                                    }



                                }


                            }

                            if(key[KEY_ESC])
                            {
                                break;
                                break;
                                break;

                            }



/////////////////////////////////////////////////////////////////
                            if(movimientos2==1)
                            {
                                for(int i=0; i<8; i++)
                                {
                                    // blit(fondo,buffer,0,0,0,0,800,600);
                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,combo2[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==1) rest(180);
                                    else rest(250);
                                }


                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov1[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);

                                if(key[KEY_ESC])
                                {
                                    break;
                                    break;
                                    break;

                                }

                                while(true)
                                {
                                    if(key[KEY_X])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_Z] && key[KEY_LEFT])
                                    {
                                        for(int i=0; i<4; i++)
                                        {
                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov1p[i],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            if(i==2 || i==3) rest(350);
                                            else rest(300);
                                        }
                                        blit(buffer2,buffer,0,0,0,0,800,600);
                                        //draw_sprite(buffer,mov8p[0],xp,yp);
                                        draw_sprite(buffer,mov1[0],x,y);
                                        blit(buffer,screen,0,0,0,0,800,600);

                                        while(true)
                                        {
                                            if(key[KEY_Z])
                                            {
                                                vidas--;
                                                break;
                                                break;
                                            }
                                            if(key[KEY_ESC])
                                            {
                                                break;
                                                break;
                                                break;
                                            }
                                            if(key[KEY_X])
                                            {
                                                puntos+=10;
                                                vidas+=1;
                                                for(int i=0; i<4; i++)
                                                {
                                                    blit(fondo,buffer,0,0,0,0,800,600);
                                                    draw_sprite(buffer,mov4p[i],xp,yp);
                                                    draw_sprite(buffer,mov1[0],x,y);
                                                    blit(buffer,screen,0,0,0,0,800,600);
                                                    if(i==2 || i==3) rest(350);
                                                    else rest(300);
                                                }

                                                blit(fondo,buffer,0,0,0,0,800,600);
                                                draw_sprite(buffer,mov4p[0],xp,yp);
                                                draw_sprite(buffer,mov1[0],x,y);
                                                blit(buffer,screen,0,0,0,0,800,600);
                                                break;

                                            }
                                        }
                                        break;
                                    }



                                }


                            }





/////////////////////////////////////////////////////////////////////
                            if(movimientos2==2)
                            {

                                for(int i=0; i<9; i++)
                                {
                                    // blit(fondo,buffer,0,0,0,0,800,600);
                                    blit(buffer2,buffer,0,0,0,0,800,600);
                                    draw_sprite(buffer,combo3[i],x,y);
                                    blit(buffer,screen,0,0,0,0,800,600);
                                    if(i==1) rest(180);
                                    else rest(250);
                                }


                                blit(buffer2,buffer,0,0,0,0,800,600);
                                draw_sprite(buffer,mov1[0],x,y);
                                blit(buffer,screen,0,0,0,0,800,600);

                                if(key[KEY_ESC])
                                {
                                    break;
                                    break;
                                    break;

                                }

                                while(true)
                                {
                                    if(key[KEY_Z])
                                    {
                                        vidas--;
                                        break;
                                    }
                                    if(key[KEY_X] && key[KEY_UP])
                                    {
                                        for(int i=0; i<2; i++)
                                        {
                                            blit(fondo,buffer,0,0,0,0,800,600);
                                            draw_sprite(buffer,mov5p[i],xp,yp);
                                            draw_sprite(buffer,mov1[0],x,y);
                                            blit(buffer,screen,0,0,0,0,800,600);
                                            if(i==2 || i==3) rest(350);
                                            else rest(300);
                                        }
                                        blit(buffer2,buffer,0,0,0,0,800,600);
                                        //draw_sprite(buffer,mov8p[0],xp,yp);
                                        draw_sprite(buffer,mov1[0],x,y);
                                        blit(buffer,screen,0,0,0,0,800,600);

                                        while(true)
                                        {
                                            if(key[KEY_X])
                                            {
                                                vidas--;
                                                break;
                                                break;
                                            }
                                            if(key[KEY_ESC])
                                            {
                                                break;
                                                break;
                                                break;
                                            }
                                            if(key[KEY_Z])
                                            {
                                                if(key[KEY_UP])
                                                {
                                                    puntos+=10;
                                                    vidas+=1;
                                                    for(int i=0; i<5; i++)
                                                    {
                                                        blit(fondo,buffer,0,0,0,0,800,600);
                                                        draw_sprite(buffer,mov6p[i],xp,yp);
                                                        draw_sprite(buffer,mov1[0],x,y);
                                                        blit(buffer,screen,0,0,0,0,800,600);
                                                        if(i==2 || i==3) rest(350);
                                                        else rest(300);
                                                    }

                                                    blit(fondo,buffer,0,0,0,0,800,600);
                                                    draw_sprite(buffer,mov6p[0],xp,yp);
                                                    draw_sprite(buffer,mov1[0],x,y);
                                                    blit(buffer,screen,0,0,0,0,800,600);
                                                    break;
                                                }
                                            }
                                        }
                                        break;
                                    }



                                }



                            }

////////////////////////////////////////////////////////////////////
                            rest(40);
                        }//finaliza el  while de la segunda parte de movimientos



                        break;
                    }//cierra if de condicion si se le da enter a la ultima imagen del arreglo de info de movimientos
                }//cierra segundo while que contiene la parte inicia del juego

                ind=1;
                c=0;
                d=0;
                puntos=0;
                vidas=2;
            }//cierra if de evento click del mouse
        }//cierra if de posicion jugar
        else
        {
            blit(inicio,buffer,0,0,0,0,800,600);
            masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,13,22);
            blit(buffer,screen,0,0,0,0,800,600);
        }


    }//fin del primer while

    readkey();
    destroy_bitmap(buffer);
    allegro_exit();
    system("main.exe");
    return 0;
}
END_OF_MAIN();



void iniciarAllegro()
{
    allegro_init();
    install_keyboard();
    install_mouse();


    set_color_depth(32);
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800, 600, 0,0);

}
